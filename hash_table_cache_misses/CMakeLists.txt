cmake_minimum_required(VERSION 3.5)

project(my_solution)

# Abseil requires C++11
set(CMAKE_CXX_STANDARD 11)

add_subdirectory(abseil-cpp)
add_subdirectory(hopscotch-map)

add_executable(solution main.cpp)

target_link_libraries(solution absl::flat_hash_map)
target_link_libraries(solution tsl::hopscotch_map)
