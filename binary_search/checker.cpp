#include "query.h"

#include <iostream>
#include <fstream>
#include <cassert>
#include <chrono>


int main(int argc, char** argv)
{
    size_t n;
    int64 maxNumber;

    assert(argc == 2);
    std::string filePath = argv[1];
    std::ifstream fileInput(filePath);

    std::vector<int64> numbers;

    fileInput >> n >> maxNumber;
    numbers.resize(n);
    for (size_t i = 0; i < n; ++i) {
        fileInput >> numbers[i];
        assert(numbers[i] >= 0 && numbers[i] < maxNumber);
    }
    STLBinarySearcher searcher(numbers);

    int64 queryCount, seed, multiplier, addend;
    fileInput >> queryCount >> seed >> multiplier >> addend;

    auto queryProcessor = QueryProcessor(maxNumber, seed, multiplier, addend);
    for (int index = 0; index < queryCount; ++index) {
        bool found = searcher.search(queryProcessor.next());
        queryProcessor.onQueryResult(found);
    }

    auto result = queryProcessor.getResult();

    std::cout << n << " " << maxNumber << std::endl;
    for (size_t i = 0; i < n; ++i) {
        std::cout << numbers[i];
        if (i + 1 < n) {
            std::cout << " ";
        } else {
            std::cout << std::endl;
        }
    }

    std::string token;
    while (true) {
        std::cerr << "Waiting solution readiness" << std::endl;
        std::getline(std::cin, token);
        if (token.empty()) {
            continue;
        }
        if (token != "READY") {
            std::cerr << "User program does not output 'READY' string" << std::endl;
            return 1;
        } else {
            break;
        }
    }

    int64 answer;

    auto startTime = std::chrono::high_resolution_clock::now();
    std::cout << queryCount << " " << seed << " " << multiplier << " " << addend << std::endl;
    std::cerr << "Waiting solution answer" << std::endl;
    std::cin >> answer;
    auto endTime = std::chrono::high_resolution_clock::now();

    if (answer != result) {
        std::cerr << "Answer is incorrect (answer: " << answer << ", expected: " << result << ")" << std::endl;
        return 1;
    }

    std::cerr << static_cast<double>(std::chrono::duration_cast<std::chrono::nanoseconds>(endTime - startTime).count()) / queryCount << std::endl;

    return 0;
}
